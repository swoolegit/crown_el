import Vue from 'vue'
import ElementUI from 'element-ui'
import App from './App.vue'
import Vuex from 'vuex'
Vue.use(Vuex);
Vue.use(ElementUI);

import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
import faker from 'vue-faker'
Vue.use(faker);

import axios from 'axios';
Vue.prototype.$axios = axios;
import store from './store'

import './theme/index.css'

import router from './router/router.js'
import BetPlugin from '@/utils/BetPlugin.vue'
Vue.use(BetPlugin);
Vue.filter("numFilter", function(value) {
    let tempVal = parseFloat(value).toFixed(3);
    let realVal = tempVal.substring(0, tempVal.length - 1);
    return realVal.replace(/^(-?\d+?)((?:\d{3})+)(?=\.\d+$|$)/, function (all, pre, groupOf3Digital) {
        return pre + groupOf3Digital.replace(/\d{3}/g, ',$&');
      });
});

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
