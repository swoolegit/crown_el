import { apiPostBet2 } from '@/api.js'
import moment from 'moment'
export const globalMixin = {
    data() {
        return {
            ocs:{
                lot_types: {1:'时时彩'
                            , 2:'十一选五'
                            , 3:'排列三'
                            , 4:'快乐十分'
                            , 6:'PK拾'
                            , 8:'快乐8'
                            , 9:'快三'
                            , 11:'PC蛋蛋'
                            , 12:'六合彩'},
                zodiac: {'鼠': [1,13,25,37,49]
                        ,'牛': [12,24,36,48]
                        ,'虎': [11,23,35,47]
                        ,'兔': [10,22,34,46]
                        ,'龙': [9,21,33,45]
                        ,'蛇': [8,20,32,44]
                        ,'马': [7,19,31,43]
                        ,'羊': [6,18,30,42]
                        ,'猴': [5,17,29,41]
                        ,'鸡': [4,16,28,40]
                        ,'狗': [3,15,27,39]
                        ,'猪': [2,14,26,38]},
                wx: {'金': [1,6,11,16,21,26,31,36,41,46]
                        ,'木': [2,7,12,17,22,27,32,37,42,47]
                        ,'水': [3,8,13,18,23,28,33,38,43,48]
                        ,'火': [4,9,14,19,24,29,34,39,44,49]
                        ,'土': [5,10,15,20,25,30,35,40,45]
					},
                pcdd_color:{
                            '红':[3,6,9,12,15,18,21,24],
                            '绿':[1,4,7,10,16,19,22,25],
                            '蓝':[2,5,8,11,17,20,23,26],
                        },
                lhc_color:{
                            '红':[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
                            '蓝':[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
                            '绿':[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49],
                        },
				LHC: {
					HONG_NUMS: [1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46],
					LANG_NUMS: [3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48],
					LV_NUMS: [5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49]
					},
                generateType:function(type_id)
                {
                    var type = 0;
                    if([1,5].indexOf(type_id)!=-1)
                    {
                        type = 1;//时时彩
                    }
                    if([6].indexOf(type_id)!=-1)
                    {
                        type = 2;//十一选五
                    }
                    if([9,10,11].indexOf(type_id)!=-1)
                    {
                        type = 3;//排列三
                    }
                    if([17].indexOf(type_id)!=-1)
                    {
                        type = 4;//快乐十分
                    }
                    if([20,61,75].indexOf(type_id)!=-1)
                    {
                        type = 6;//PK拾
                    }
                    if([24,68].indexOf(type_id)!=-1)
                    {
                        type = 8;//快乐8
                    }
                    if([25,60,94].indexOf(type_id)!=-1)
                    {
                        type = 9;//快三
                    }
                    if([65,68].indexOf(type_id)!=-1)
                    {
                        type = 11;//PC蛋蛋
                    }
                    if([90].indexOf(type_id)!=-1)
                    {
                        type = 12;//六合彩
                    }
                    return type;
                },
                generateById: function(type_id, oc){
                        var type = 0;
                        type=this.generateType(type_id);
                        /*
                        if($.inArray(type_id, [1, 8, 17]) > -1){	// 时时彩
                            type = 1;
                        }else if($.inArray(type_id, [2]) > -1){	// 十一选五
                            type = 2;
                        }else if($.inArray(type_id, [3, 10, 13, 14, 15]) > -1){	// 快三
                            type = 3;
                        }else if($.inArray(type_id, [4, 9, 11, 16]) > -1){	// PK拾
                            type = 4;
                        }else if($.inArray(type_id, [5]) > -1){	// PC蛋蛋
                            type = 5;
                        }else if($.inArray(type_id, [6, 12, 18]) > -1){	// 快乐十分
                            type = 6;
                        }else if($.inArray(type_id, [7]) > -1){	// 六合彩
                            type = 7;
                        }else if($.inArray(type_id, [19, 20]) > -1){	// 3D, P3
                            type = 8;
                        } */
                        //console.log(this.zodiac,this.zodiac);
                        return this.generate(type, oc);
                    },
                generate: function(type, oc){
                    var tmp;
                    tmp = oc.split('+');
                    var sp = '';
                    if(tmp.length > 1){
                        sp = tmp[1];
                    }
                    var codes = tmp[0].split(',');
					var i = 1;
					var data = {};
					for(var key in codes){
						data['c' + i] = codes[key];
						i++;
					};
                    if(type == 1){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]);
                        data['total'] = total;
                        var totalDx = total >= 23 ? "大": "小";
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else if (parseInt(codes[0]) == parseInt(codes[4])) {
                            lh = "和";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;
						data['q3'] = this.getZFB([ data.c1, data.c2, data.c3 ]);
						data['z3'] = this.getZFB([ data.c2, data.c3, data.c4 ]);
						data['h3'] = this.getZFB([ data.c3, data.c4, data.c5 ]);
                    }else if(type == 2){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]);
                        data['total'] = total;
                        var totalDx = "小";
                        if(total == 30){
                            totalDx = "和";
                        }else  if (total > 30) {
                            totalDx = "大";
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var totalWs = total % 10 >= 5 ? "尾大": "尾小";
                        data['totalWs'] = totalWs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;

						var dx1 = "小";
						if (data.c1 == 11) {
							dx1 = "和";
						} else if (data.c1 > 5) {
							dx1 = "大";
						}
						data['dx1'] = dx1;
						var dx2 = "小";
						if (data.c2 == 11) {
							dx2 = "和";
						} else if (data.c2 > 5) {
							dx2 = "大";
						}
						data['dx2'] = dx2;
						var dx3 = "小";
						if (data.c3 == 11) {
							dx3 = "和";
						} else if (data.c3 > 5) {
							dx3 = "大";
						}
						data['dx3'] = dx3;
						var dx4 = "小";
						if (data.c4 == 11) {
							dx4 = "和";
						} else if (data.c4 > 5) {
							dx4 = "大";
						}
						data['dx4'] = dx4;
						var dx5 = "小";
						if (data.c5 == 11) {
							dx5 = "和";
						} else if (data.c5 > 5) {
							dx5 = "大";
						}
						data['dx5'] = dx5;
                    }else if(type == 9 || type == 11){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
                        data['total'] = total;
                        var totalDx = "";
                        if(type == 9){
                            if (total >= 11) {
                                totalDx = "大";
                            } else {
                                totalDx = "小";
                            }
                        }else{
                            if (total >= 14) {
                                totalDx = "大";
                            } else {
                                totalDx = "小";
                            }
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
						if(type == 11)
						{
							if(total >= 23){
								data['jz'] = "極大";
							}else if(total <= 4){
								data['jz'] = "極小";
							}else{
								data['jz'] = "--";
							}
                            for(var key2 in this.pcdd_color){
                                if(this.pcdd_color[key2].indexOf(total)!=-1)
                                {
                                    data['color'] = key2;
                                    break;
                                }
                            }
						}else
						{
							data['xt'] = this.getZFBk3([ data.c1, data.c2, data.c3 ]);
						}

                    }else if(type == 6){
                        var gyh = parseInt(codes[0]) + parseInt(codes[1]);	// 冠亚和
                        data['gyh'] = gyh;
                        var gydx = gyh == 11?"和":gyh > 11?"大":"小";	// 冠亚大小
                        data['gydx'] = gydx;
                        var gyds = gyh % 2 == 0 ? "双": "单";	// 冠亚单双
                        data['gyds'] = gyds;
                        var lh1 = parseInt(codes[0]) > parseInt(codes[9]) ? "龙": "虎";
                        data['lh1'] = lh1;
                        var lh2 = parseInt(codes[1]) > parseInt(codes[8]) ? "龙": "虎";
                        data['lh2'] = lh2;
                        var lh3 = parseInt(codes[2]) > parseInt(codes[7]) ? "龙": "虎";
                        data['lh3'] = lh3;
                        var lh4 = parseInt(codes[3]) > parseInt(codes[6]) ? "龙": "虎";
                        data['lh4'] = lh4;
                        var lh5 = parseInt(codes[4]) > parseInt(codes[5]) ? "龙": "虎";
                        data['lh5'] = lh5;
                    }else if(type == 4){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]) + parseInt(codes[5]) + parseInt(codes[6]) + parseInt(codes[7]);
                        data['total'] = total;
                        var totalDx = "";
                        if (total > 84) {
                            totalDx = "大";
                        } else if (total == 84) {
                            totalDx = "和";
                        } else if (total < 84) {
                            totalDx = "小";
                        }
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var wsDesc = "";
                        if (total % 10 < 5) {
                            wsDesc = "尾小";
                        } else {
                            wsDesc = "尾大";
                        }
                        data['wsDesc'] = wsDesc;
						var lh1 = parseInt(codes[0]) > parseInt(codes[7]) ? "龍": "虎";
						data['lh1'] = lh1;
						var lh2 = parseInt(codes[1]) > parseInt(codes[6]) ? "龍": "虎";
						data['lh2'] = lh2;
						var lh3 = parseInt(codes[2]) > parseInt(codes[5]) ? "龍": "虎";
						data['lh3'] = lh3;
						var lh4 = parseInt(codes[3]) > parseInt(codes[4]) ? "龍": "虎";
						data['lh4'] = lh4;
                    }else if(type == 12){
                            for(var key in codes){
                                var val = parseInt(codes[key]);
                                for(var key2 in this.zodiac){
                                    if(this.zodiac[key2].indexOf(val)!=-1){
                                        data['z_' + val] = key2;
                                        break;
                                    }
                                }
                            };
						var sp = parseInt(codes[6]);
                        // data['split'] = '+';
                        // var val = parseInt(sp);
                        // for(var key2 in this.zodiac){
                        //     if(this.zodiac[key2].indexOf(val)!=-1){
                        //     //if($.inArray(val, ocs.zodiac[key2]) != -1){
                        //         data['z_' + val] = key2;
                        //         break;
                        //     }
                        // }
						// var val = parseInt(codes.c7);
						// for(var key2 in this.zodiac){
						// 	for(var key3 in this.zodiac[key2]){
						// 		if(this.zodiac[key2][key3].indexOf(val)!=-1){
						// 			data['zsp'] = key2;
						// 			break;
						// 		}
						// 	}
						// 	// if($.inArray(val, ocs.zodiac[key2]) != -1){
						// 	// 	data['zsp'] = key2;
						// 	// 	break;
						// 	// }
						// }
						var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]) + parseInt(codes[3]) + parseInt(codes[4]) + parseInt(codes[5]) + parseInt(codes[6]);
						data['total'] = total;
						data['totalDs'] = total % 2 == 0?"雙":"單";
						data['totalDx'] = "小";
						if (total >= 175) {
							data['totalDx'] = "大";
						}
						var totalQsb = "";
						var nums = [ parseInt(codes[0]), parseInt(codes[1]), parseInt(codes[2]), parseInt(codes[3]), parseInt(codes[4]), parseInt(codes[5]) ];
						var hongNum = 0;
						var langNum = 0;
						var lvNum = 0;
						for ( var k = 0;k < nums.length;k++) {
							var num1 = nums[k];
							if (this.ArrayContains(this.LHC.HONG_NUMS, num1)) {
								hongNum += 1;
							}
							if (this.ArrayContains(this.LHC.LANG_NUMS, num1)) {
								langNum += 1;
							}
							if (this.ArrayContains(this.LHC.LV_NUMS, num1)) {
								lvNum += 1;
							}
						}
						if ((langNum == 3 && lvNum == 3 && this.ArrayContains(this.LHC.HONG_NUMS, parseInt(sp))) || (langNum == 3 && hongNum == 3 && this.ArrayContains(this.LHC.LV_NUMS, parseInt(sp))) || (lvNum == 3 && hongNum == 3 && this.ArrayContains(this.LHC.langNum, parseInt(sp)))){// 和局
							totalQsb = "和局";
						}else{
							var n = parseInt(sp);
							if (this.ArrayContains(this.LHC.HONG_NUMS, n)) {
								hongNum += 1.5;
							}
							if (this.ArrayContains(this.LHC.LANG_NUMS, n)) {
								langNum += 1.5;
							}
							if (this.ArrayContains(this.LHC.LV_NUMS, n)) {
								lvNum += 1.5;
							}

							if (hongNum > langNum && hongNum > lvNum) {
								totalQsb = "紅";
							} else if (langNum > hongNum && langNum > lvNum) {
								totalQsb = "藍";
							} else if (lvNum > langNum && lvNum > hongNum) {
								totalQsb = "綠";
							}
						}
						data['qsb'] = totalQsb;

						if(sp == 49){
							data['tmDs'] = "和";
						}else if( sp % 2 == 0){
							data['tmDs'] = "雙";
						}else{
							data['tmDs'] = "單";
						}

						if(sp == 49){
							data['tmDx'] = "和";
						}else if(sp >= 25){
							data['tmDx'] = "大";
						}else{
							data['tmDx'] = "小";
						}

						var tmTotal;
						if(sp < 10){
							tmTotal = sp;
						}else{
							tmTotal = parseInt(sp / 10) + sp % 10;
						}
						if(sp == 49){
							data['tmHds'] = "和";
						}else if(tmTotal %2 == 0){
							data['tmHds'] = "雙";
						}else{
							data['tmHds'] = "單";
						}

						if(sp == 49){
							data['tmHdx'] = "和";
						}else if(tmTotal>=7){
							data['tmHdx'] = "大";
						}else{
							data['tmHdx'] = "小";
						}

						if(sp == 49){
							data['tmDxw'] = "和";
						}else if(sp % 10 >= 5){
							data['tmDxw'] = "尾大";
						}else{
							data['tmDxw'] = "尾小";
						}
                    }else if(type == 3){
                        var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
                        data['total'] = total;
                        var totalDx = total >= 14 ? "大": "小";
                        data['totalDx'] = totalDx;
                        var totalDs = total % 2 == 0 ? "双": "单";
                        data['totalDs'] = totalDs;
                        var lh = "";
                        if (parseInt(codes[0]) > parseInt(codes[4])) {
                            lh = "龙";
                        } else if (parseInt(codes[0]) == parseInt(codes[4])) {
                            lh = "和";
                        } else {
                            lh = "虎";
                        }
                        data['lh'] = lh;
						data['q3'] = this.getZFB([ data.c1, data.c2, data.c3 ]);
                    }
					for(var i=1;i<=codes.length;i++){
						delete data['c' + i];
					};
                    return data;
                },
				isSameNum: function(nums) {
					var num1 = nums[0];
					for (var i = 1; i < nums.length; i++) {
						if (num1 != nums[i]) {
							return false;
						}
					}
					return true;
				},
				isPair: function(nums) {
					for (var i = 0; i < nums.length; i++) {
						var code = nums[i];
						for (var j = i + 1; j < nums.length; j++) {
							var codeTemp = nums[j];
							if (code == codeTemp) {
								return true;
							}
						}
					}
					return false;
				},
				isStraight: function(nums) {
						nums.sort();
						if (nums[0] == 0 && nums[1] == 1 && nums[2] == 9) {// 019也算順子
							return true;
						}
						if (nums[0] == 0 && nums[1] == 8 && nums[2] == 9) {// 089也算順子
							return true;
						}
						for (var i = 0; i < nums.length - 1; i++) {
							var num1 = nums[i];
							var num2 = nums[i + 1];
							if (num2 - num1 != 1) {
								return false;
							}
						}
						return true;
					},
				isHalfStraight: function(nums) {
					nums.sort();
					var n = 0;
					if (nums[0] == 0) {
						n += 1;
					}
					if (nums[1] == 1) {
						n += 1;
					}
					if (nums[2] == 9) {
						n += 1;
					}
					if (n == 2) {
						return true;
					}
					for (var i = 0; i < nums.length - 1; i++) {
						var num1 = nums[i];
						var num2 = nums[i + 1];
						if (num2 - num1 == 1) {
							return true;
						}
					}
					return false;
				},
				ArrayContains: function(array, num) {
					for ( var i in array) {
						if (array[i] == num) {
							return true;
						}
					}
					return false;
				},
				getZFB : function(nums) {
					var str = "";
					if (this.isSameNum(nums)) {
						str = "豹子";
					} else if (this.isPair(nums)) {
						str = "對子";
					} else if (this.isStraight(nums)) {
						str = "順子";
					} else if (this.isHalfStraight(nums)) {
						str = "半順";
					} else {
						str = "雜六";
					}
					return str;
				},
				getZFBk3 : function(nums) {
					var str = "";
					if (this.isSameNum(nums)) {
						str = "豹子";
					} else if (this.isPair(nums)) {
						str = "對子";
					} else {
						str = "三不同";
					}
					return str;
				},
            },
			loadOptions:{
				fullscreen: true,
				lock: true,
				text: 'Loading',
				spinner: 'el-icon-loading',
				background: 'rgba(0, 0, 0, 0.7)'
			},
			MaintainOptions:{
				fullscreen: true,
				lock: true,
				//text: 'Loading',
				//spinner: 'el-icon-loading',
				background: 'rgba(0, 0, 0, 0.7)'
			},
			DatePickerOptions: {
				shortcuts: [{
					text: '最近一周',
					onClick(picker) {
					  const end = new Date();
					  const start = new Date();
					  start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
					  picker.$emit('pick', [start, end]);
					}
				}, {
					text: '最近一个月',
					onClick(picker) {
					  const end = new Date();
					  const start = new Date();
					  start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
					  picker.$emit('pick', [start, end]);
					}
				}, {
					text: '最近三个月',
					onClick(picker) {
					  const end = new Date();
					  const start = new Date();
					  start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
					  picker.$emit('pick', [start, end]);
					}
				}]
			},
        };
    },
    methods: {
		stopLoad()
		{
			this.loading = false;
			this.noMore=true;
			this.disabled=true;
		},
        go2page(url){
			this.$router.push({path: url, query: {}});
        },
        str_split(str)
        {
			str = str.toString();
            return str.split(",");
        },
        time_split(str)
        {
			var txt=str.toString();
			if(txt=="0")
			{
				txt=="00";
			}
			if(txt)
			{
				return [txt.substr(0,1),txt.substr(1,1)];
			}else
			{
				return ["0","0"];
			}
        },
        numberPk10(value)
        {
            return parseInt(value);
        },
        checkPosNum(num)
        {
            if (num < 0)
            {
                return false;
            }else
            {
                return true;
            }
        },
        error_func(errID){
            switch(errID)
            {
                case 1:
                    this.$store.dispatch('clearUserInfo');
                    return;
            }
        },
		message(txt)
		{
			this.$message(txt);
		},
        toast(msg, events){
			this.$alert(msg, '提示', {
			  confirmButtonText: '确定',
			  callback: action => {
				events
			  }
			});

        },
        back() {
            this.$router.back()
        },
		changeAmount(e){
			var target=e.target;
			target=e.target.parentElement;
			target=target.parentElement;
			var str=target.className;
			var playedId=target.dataset.id;
			if(this.betData.length>0)
			{
				this.delBet(playedId);
			}
			if(e.target.value!="")
			{
				var Data={
					playedId:playedId,
					betAmount:e.target.value,
				};
				this.$store.commit('AddbetData',Data);
				this.$store.commit('AddbetFlag',{"playedId":playedId,"target":target});
				if(str.indexOf("name")>-1 && str.indexOf("addbet")==-1)
				{
					target.className=str+" addbet";
				}
			}else
			{
				target.className=str.replace(" addbet", "");
			}
		},
        addBet(e)
        {
            var str=e.target.className;
            var target=e.target;
            if(str=="" || str=="game-title" || str.indexOf("icon-title")>-1 || str.indexOf("play-menu")>-1 || str.indexOf("credit_bet")>-1 || str.indexOf("input-amount")>-1 || str.indexOf("el-aside")>-1 || str.indexOf("fas")>-1 || str.indexOf("fas")>-1){return;}
            if(str.indexOf("balls")>-1 || str.indexOf("square")>-1 || str.indexOf("pl")>-1 || str.indexOf("k3")>-1 || str.indexOf("fonts")>-1)
            {
                target=e.target.parentElement;
                str=target.className;
            }
			var input=target;
			if(str.indexOf("el-input__inner")>-1)
			{
				target=e.target.parentElement;
				target=target.parentElement;
				str=target.className;
				if(input.value=="")
				{
					input.value=this.betAmount;
				}
			}else
			{
				if(str.indexOf("five-balls-space")>-1 || str.indexOf("ten-balls-space")>-1)
				{
					target=e.target.parentElement;
					target=target.parentElement;
					str=target.className;
				}
				input=target.childNodes[target.childNodes.length-1].childNodes[1];
				input.value=this.betAmount;
			}

			var playedId=target.dataset.id;
            if(str.indexOf("name")>-1 && str.indexOf("addbet")>-1)
            {
                target.className=str.replace(" addbet", "");
            }
            if(str.indexOf("name")>-1 && str.indexOf("addbet")==-1)
            {
                target.className=str+" addbet";
            }
            if(this.betData.length>0)
            {
                if(this.delBet(playedId))
                {
					input.value="";
                    return;
                }
            }
            var Data={
                playedId:playedId,
				betAmount:input.value,
            };
            this.$store.commit('AddbetData',Data);
            this.$store.commit('AddbetFlag',{"playedId":playedId,"target":target});
            return;
        },
        addBet_4d(e)
        {
            var str=e.target.className;
            var target=e.target;
            if(str=="" || str=="game-title" || str.indexOf("icon-title")>-1 || str.indexOf("play-menu")>-1 || str.indexOf("credit_bet")>-1 || str.indexOf("input-amount")>-1 || str.indexOf("el-aside")>-1 || str.indexOf("fas")>-1 || str.indexOf("fas")>-1){return;}
            if(str.indexOf("balls")>-1 || str.indexOf("square")>-1 || str.indexOf("pl")>-1 || str.indexOf("k3")>-1 || str.indexOf("fonts")>-1)
            {
                target=e.target.parentElement;
                str=target.className;
            }

			var playedId=target.dataset.id;
            if(str.indexOf("name")>-1 && str.indexOf("addbet")>-1)
            {
                target.className=str.replace(" addbet", "");
            }
            if(str.indexOf("name")>-1 && str.indexOf("addbet")==-1)
            {
                target.className=str+" addbet";
            }
            if(this.betData.length>0)
            {
                if(this.delBet(playedId))
                {
					//input.value="";
                    return;
                }
            }
            var Data={
                playedId:playedId,
				betAmount:this.betAmount,
            };
            this.$store.commit('AddbetData',Data);
            this.$store.commit('AddbetFlag',{"playedId":playedId,"target":target});
            return;
        },
        delBet(playedId)
        {
            var check =false;
            for (var i= 0; i< this.betData.length; i++){
                if(this.betData[i].playedId==playedId)
                {
                    this.$store.commit('DelbetData',i);
                    check = true;
                    break;
                }
            }
            for (var i= 0; i< this.betFlag.length; i++){
                if(this.betFlag[i].playedId==playedId)
                {
                    this.$store.commit('DelbetFlag',i);
                    check = true;
                    break;
                }
            }
            if(check==true)
            {
                return true;
            }
        },
        restBet()
        {
            if(this.betFlag.length > 0)
            {
                for (var i= 0; i< this.betFlag.length; i++){
                    var target = this.betFlag[i].target;
					var input=target;
                    var str=target.className;
                    str=str.replace(" addbet", "");
                    target.className=str.replace(" addOF", "");
					input=target.childNodes[target.childNodes.length-1].childNodes[1];
					if(input)
					{
						input.value="";
					}
                }
            }
            this.$store.commit('RestBet');
        },
        postBet()
        {
            var code=[];
            if(this.betData.length <=0)
            {
                this.message("请先选择号码");
                return;
            }
            if(this.currentIssue ==null)
            {
                this.message("尚未获取奖期");
                return;
            }
            if(this.user.username =='')
            {
                this.message("请先登录");
                return;
            }
			let betAmount = 0;
            for (var i= 0; i< this.betData.length; i++){
                var playedId=this.betData[i].playedId;
				if(parseInt(this.betData[i].betAmount) < this.config.min_bet)
				{
					this.message("单笔投注金额不得小于 " + this.config.min_bet);
					return;
				}
				betAmount += parseInt(this.betData[i].betAmount);
                code.push({
                    fanDian:0,
                    bonusProp:this.mapByPlay[playedId].bp,
                    actionData:this.mapByPlay[playedId].name,
                    playedGroup:this.mapByPlay[playedId].groupId,
                    playedId:playedId,
                    actionAmount:this.betData[i].betAmount,
                    type:this.type_id,
                    //orderId:this.$faker().random.uuid(),
                });
            }
			if(betAmount > this.user.coin)
			{
                this.message("錢包餘額不足,請先充值");
                return;
			}
			let loadingInstance = this.$loading(this.loadOptions);
            apiPostBet2({
                code : code,
                para:{
                    type:this.type_id,
                    actionNo:this.currentIssue.actionNo,
                    kjTime:this.currentIssue.actionTime,
                    betType:0
                },
            })
            .then(response => {
				loadingInstance.close();
                if(response.data.error)
                {
                    this.error_handler(response.data);
                }
                if(response.data.status)
                {
                    this.message(response.data.msg);
					this.$store.dispatch('setCoin',response.data.coin);
                }
                return;
            }).catch(error => {
				loadingInstance.close();
                console.log(error);
                return;
            });
        },
        error_handler(e)
        {
            // if(e.func==2)
            // {
            //     this.toast(e.error,null,0);
            //     return;
            // }else
            // {
            //     this.toast(e.error);
            // }
            if(e.func)
			{
				switch(e.func)
				{
					case 1:
						this.toast(e.error);
						this.$store.dispatch('clearUserInfo');
						this.go2page("/");
						return;
					break;
					case 2:
						this.toast(e.error);
						this.go2page("/");
						return;
					break;
					case 3:
						this.$alert(e.error, '提示', {
						  confirmButtonText: '确定',
						  callback: action => {
							this.$router.go(0);
						  }
						});
						//this.toast(e.error);
						return;
					break;
				}
			}
			this.toast(e.error);
            return;
        },
        getStatus(item){
            if(item.isDelete==1)
            {
                return "已撤单";
            }
            if(item.lotteryNo!="")
            {
                return "已开奖";
            }
            if(item.lotteryNo=="")
            {
                return "未开奖";
            }
        },
        getCashStatus(item){
            switch(item.state)
            {
                case 1:
                    return "申请中";
                case 2:
                    return "已取消";
                case 3:
                    return "已出款";
                case 4:
                    return "提款失败";
                case 0:
                    return "已出款";
            }
        },
        getRechargeStatus(item){
            switch(item.state)
            {
                case 0:
                    return "申请中";
                case 1:
                    return "成功到账";
                case 2:
                    return "存款失败";
            }
        },
        getClientType(type){
            switch(type)
            {
                case 0:
                    return "PC端";
                case 1:
                    return "手机端";
            }
        },
        getMomentFullDate(time)
        {
			if(time=="")
			{
				return "--";
			}
            time=time*1000;
            return moment(time).format("YYYY-MM-DD HH:mm");
        },
        getMomentYearDate(time)
        {
            time=time*1000;
            return moment(time).format("YYYY-MM-DD");
        },
        getMomentDate(time)
        {
            time=time*1000;
            return moment(time).format("MM-DD");
        },
        getMomentTime(time)
        {
            time=time*1000;
            return moment(time).format("HH:mm");
        },
        getMomentDTDate(time)
        {
			if(time=="")
			{
				return "--";
			}
            time=time*1000;
            return moment(time).format("MM-DD HH:mm");
        },
        pcdd_code(str)
        {
            var codes = str.split(',');
            var total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
            for(var key2 in this.pcdd_color){
                if(this.pcdd_color[key2].indexOf(total)!=-1)
                {
                    data['color'] = key2;
                    break;
                }
            }
            return total;
        },
        pcdd_color(str)
        {
            var codes = str.split(',');
            var total=0;
            if(codes.length==1)
            {
                total=parseInt(codes[0]);
            }else
            {
                total = parseInt(codes[0]) + parseInt(codes[1]) + parseInt(codes[2]);
            }
            for(var key2 in this.ocs.pcdd_color){
                if(this.ocs.pcdd_color[key2].indexOf(total)!=-1)
                {
                    if(key2=="红")
                    {
                        return "pcdd_red";
                    }
                    if(key2=="绿")
                    {
                        return "pcdd_green";
                    }
                    if(key2=="蓝")
                    {
                        return "pcdd_blue";
                    }
                }
            }
            return "pcdd_no";
        },
        lhc_color(str)
        {
            var total=parseInt(str);
            for(var key2 in this.ocs.lhc_color){
                if(this.ocs.lhc_color[key2].indexOf(total)!=-1)
                {
                    if(key2=="红")
                    {
                        return "lhc_bg_red";
                    }
                    if(key2=="绿")
                    {
                        return "lhc_bg_green";
                    }
                    if(key2=="蓝")
                    {
                        return "lhc_bg_blue";
                    }
                }
            }
        },
		gethms(time)
		{
			let msec = 0;
			let day = 0;
			let hr = 0;
			let min = 0;
			let sec = 0;
			msec = time;
			day = parseInt(msec / 60 / 60 / 24);
			hr = parseInt(msec  / 60 / 60 % 24);
			min = parseInt(msec / 60 % 60);
			sec = parseInt(msec % 60);
			return {
				day:day > 9 ? day : '0' + day,
				hr:hr > 9 ? hr : '0' + hr,
				min:min > 9 ? min : '0' + min,
				sec:sec > 9 ? sec : '0' + sec
			}
		},
		getSortNo(str)
		{
			var action = str.toString();
			if(action.length > 10)
			{
				return "...."+action.substr(action.length-4);
			}
			return action;
		}
/*         noCode(num,min,max)
        {
            var code="";
            for(var i=0;i<num;i++)
            {
                code=code + "," + (Math.floor(Math.random()*max)+min);
            }
            return code.substr(1);
        },    */
    },

}
