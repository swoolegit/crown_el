import GlobalApp from './GlobalApp'
export default {
    config: GlobalApp.config,
	user:{
		uid : 0,
		username : '',
        coin:0,
        type : 0,
        score:0,
        grade:0,
        fanDian:0,
        forTest:0,
    },
    coin_time_line:null,
    betData:[], //注单内容
    betFlag:[], //标记选中号码
	beiShu:'1', //倍數(官方玩法使用)
    betCfg:{
        1:false,
        2:false,
        3:false,
        4:false,
        6:false,
        8:false,
        9:false,
        11:false,
        12:false,
		13:false,
		14:false,
		15:false,
		16:false,
		17:false,
		18:false,
    },
    betGroupCheck:{
        1:false,
        2:false,
        3:false,
        4:false,
        6:false,
        8:false,
        9:false,
        11:false,
        12:false,
		13:false,
		14:false,
		15:false,
		16:false,
		17:false,
		18:false,
    },
	betAmount:10, //下注金額
    mapByType:{}, //彩种
    mapByPlay:{}, //赔率表
    mapByGroup:{}, //玩法群(官方玩法使用)
    mapByGroupToPlay:{}, //玩法群(官方玩法使用)
	mapByEgame:{},
    currentIssue:null,
    lastIssue:null,
    betCountdown:{
        hr:0,
        min:0,
        sec:0,
    },
    raceCountdown:0,
	switchPlay:'信用', //預設玩法
	CartData:[],
}
