import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const router = new Router({
	//mode: 'history',
	routes: [
		{
			path: '/',
			component: () => import('@/page/layout/Default.vue'),
			children: [
				{
					path: '',
					name: 'home',
					component: () => import('@/page/index/index.vue'),
				},
				{
					path: 'help',
					name: 'help',
					component: () => import('@/page/index/help.vue'),
				},
				{
					path: 'promotion',
					name: 'promotion',
					component: () => import('@/page/index/promotion.vue'),
				},
				{
					path: 'egame',
					name: 'egame',
					component: () => import('@/page/index/egame.vue'),
				},
				{
					path: 'poker',
					name: 'poker',
					component: () => import('@/page/index/poker.vue'),
				},
				{
					path: 'app',
					name: 'app',
					component: () => import('@/page/index/app.vue'),
				},
				{
					path: 'history',
					name: 'history',
					component: () => import('@/page/index/history.vue'),
				},
				{
					path: 'bulletin',
					name: 'bulletin',
					component: () => import('@/page/index/bulletin.vue'),
				},
				{
					path: 'spreg/:id',
					name: 'spreg',
					component: () => import('@/page/index/index.vue'),
				}
			]
		},
		{
			path: '/game',
			component: () => import('@/page/layout/GameDefault.vue'),
			children: [
				{
					path: '',
					component: () => import('@/page/game/index.vue'),
				},
			]
		},
		{
			path: '/game/credit',
			component: () => import('@/page/layout/GameDefault.vue'),
			children: [
				{
					path: 'ld', //老捣
					component: () => import('@/page/game/credit/ld.vue'),
				},
				{
					path: 'cqssc', //重庆时时彩
					component: () => import('@/page/game/credit/cqssc.vue'),
				},
				{
					path: 'jcssc', //極速时时彩
					component: () => import('@/page/game/credit/jcssc.vue'),
				},
				{
					path: 'gd11x5', //廣東11選5
					component: () => import('@/page/game/credit/gd11x5.vue'),
				},
				{
					path: 'fc3d', //福彩3D
					component: () => import('@/page/game/credit/fc3d.vue'),
				},
				{
					path: 'pl3', //排列3
					component: () => import('@/page/game/credit/pl3.vue'),
				},
				{
					path: 'gdklsf', //廣東快樂10分
					component: () => import('@/page/game/credit/gdklsf.vue'),
				},
				{
					path: 'xync', //幸运农场
					component: () => import('@/page/game/credit/xync.vue'),
				},
				{
					path: 'pk10', //北京賽車PK10
					component: () => import('@/page/game/credit/pk10.vue'),
				},
				{
					path: 'jsk3', //江苏骰宝
					component: () => import('@/page/game/credit/jsk3.vue'),
				},
				{
					path: 'gxk3', //广西快3
					component: () => import('@/page/game/credit/gxk3.vue'),
				},
				{
					path: 'jck3', //极速快三
					component: () => import('@/page/game/credit/jck3.vue'),
				},
				{
					path: 'xyft', //幸运飞艇
					component: () => import('@/page/game/credit/xyft.vue'),
				},
				{
					path: 'pcdd', //PC蛋蛋
					component: () => import('@/page/game/credit/pcdd.vue'),
				},
				{
					path: 'jcpk10', //極速賽車
					component: () => import('@/page/game/credit/jcpk10.vue'),
				},
				{
					path: 'jc3k3', //三分快3
					component: () => import('@/page/game/credit/jc3k3.vue'),
				},
				{
					path: 'lhc', //香港六合彩
					component: () => import('@/page/game/credit/lhc.vue'),
				},
				{
					path: 'jclhc', //極速六合彩
					component: () => import('@/page/game/credit/jclhc.vue'),
				},
				{
					path: 'jc3lhc', //三分六合彩
					component: () => import('@/page/game/credit/jc3lhc.vue'),
				},
				{
					path: 'jx11x5', //江西11选5
					component: () => import('@/page/game/credit/jx11x5.vue'),
				},
				{
					path: 'jlk3', //吉林快3
					component: () => import('@/page/game/credit/jlk3.vue'),
				},
				{
					path: 'tjssc', //天津时时彩
					component: () => import('@/page/game/credit/tjssc.vue'),
				},
				{
					path: 'tcssc', //腾讯分分彩
					component: () => import('@/page/game/credit/tcssc.vue'),
				},
				{
					path: 'jc5ssc', //加拿大五分彩
					component: () => import('@/page/game/credit/jc5ssc.vue'),
				},
				{
					path: 'jc3pk10', //三分赛车
					component: () => import('@/page/game/credit/jc3pk10.vue'),
				},
				{
					path: 'jc5pk10', //五分赛车
					component: () => import('@/page/game/credit/jc5pk10.vue'),
				},
				{
					path: 'tcpk10', //腾讯赛车
					component: () => import('@/page/game/credit/tcpk10.vue'),
				},
				{
					path: 'jc30sssc', //30秒时时彩
					component: () => import('@/page/game/credit/jc30sssc.vue'),
				},
				{
					path: 'jc30spk10', //30秒賽車
					component: () => import('@/page/game/credit/jc30spk10.vue'),
				},
				{
					path: 'jc30sk3', //30秒快三
					component: () => import('@/page/game/credit/jc30sk3.vue'),
				},
				{
					path: 'jc3ssc', //加拿大三分彩
					component: () => import('@/page/game/credit/jc3ssc.vue'),
				},
				{
					path: 'jcca1ssc', //加拿大一分彩
					component: () => import('@/page/game/credit/jcca1ssc.vue'),
				},
				{
					path: 'zh-at', //字花
					component: () => import('@/page/game/credit/zh_flower.vue'),
				},
				{
					path: 'Macaolhc', //澳门六合彩
					component: () => import('@/page/game/credit/Macaolhc.vue'),
				},
				{
					path: 'Max4D', //Max4D
					component: () => import('@/page/game/credit/max4d.vue'),
				},
				{
					path: 'jcxyft', //极速飞艇
					component: () => import('@/page/game/credit/jcxyft.vue'),
				},
				{
					path: 'jc3xyft', //三分飞艇
					component: () => import('@/page/game/credit/jc3xyft.vue'),
				},
			]
		},
		{
			path: '/game/official',
			component: () => import('@/page/layout/GameDefault.vue'),
			children: [
				{
					path: 'cqssc', //重庆时时彩
					component: () => import('@/page/game/official/cqssc.vue'),
				},
				{
					path: 'jcssc', //極速时时彩
					component: () => import('@/page/game/official/jcssc.vue'),
				},
				{
					path: 'tcssc', //腾讯分分彩
					component: () => import('@/page/game/official/tcssc.vue'),
				},
				{
					path: 'gd11x5', //廣東11選5
					component: () => import('@/page/game/official/gd11x5.vue'),
				},
				{
					path: 'fc3d', //福彩3D
					component: () => import('@/page/game/official/fc3d.vue'),
				},
				{
					path: 'pl3', //排列3
					component: () => import('@/page/game/official/pl3.vue'),
				},
				{
					path: 'gdklsf', //廣東快樂10分
					component: () => import('@/page/game/official/gdklsf.vue'),
				},
				{
					path: 'xync', //幸运农场
					component: () => import('@/page/game/official/xync.vue'),
				},
				{
					path: 'pk10', //北京賽車PK10
					component: () => import('@/page/game/official/pk10.vue'),
				},
				{
					path: 'jsk3', //江苏骰宝
					component: () => import('@/page/game/official/jsk3.vue'),
				},
				{
					path: 'gxk3', //广西快3
					component: () => import('@/page/game/official/gxk3.vue'),
				},
				{
					path: 'jck3', //极速快三
					component: () => import('@/page/game/official/jck3.vue'),
				},
				{
					path: 'xyft', //幸运飞艇
					component: () => import('@/page/game/official/xyft.vue'),
				},
				{
					path: 'jcpk10', //極速賽車
					component: () => import('@/page/game/official/jcpk10.vue'),
				},
				{
					path: 'jc3k3', //三分快3
					component: () => import('@/page/game/official/jc3k3.vue'),
				},
				{
					path: 'jx11x5', //江西11选5
					component: () => import('@/page/game/official/jx11x5.vue'),
				},
				{
					path: 'jlk3', //吉林快3
					component: () => import('@/page/game/official/jlk3.vue'),
				},
				{
					path: 'tjssc', //天津时时彩
					component: () => import('@/page/game/official/tjssc.vue'),
				},
				{
					path: 'jc5ssc', //加拿大五分彩
					component: () => import('@/page/game/official/jc5ssc.vue'),
				},
				{
					path: 'jc3pk10', //三分赛车
					component: () => import('@/page/game/official/jc3pk10.vue'),
				},
				{
					path: 'jc5pk10', //五分赛车
					component: () => import('@/page/game/official/jc5pk10.vue'),
				},
				{
					path: 'tcpk10', //腾讯赛车
					component: () => import('@/page/game/official/tcpk10.vue'),
				},
				{
					path: 'jc30sssc', //30秒时时彩
					component: () => import('@/page/game/official/jc30sssc.vue'),
				},
				{
					path: 'jc30spk10', //30秒賽車
					component: () => import('@/page/game/official/jc30spk10.vue'),
				},
				{
					path: 'jc30sk3', //30秒快三
					component: () => import('@/page/game/official/jc30sk3.vue'),
				},
				{
					path: 'jc3ssc', //加拿大三分彩
					component: () => import('@/page/game/official/jc3ssc.vue'),
				},
				{
					path: 'jcca1ssc', //加拿大一分彩
					component: () => import('@/page/game/official/jcca1ssc.vue'),
				},
				{
					path: 'jcxyft', //极速飞艇
					component: () => import('@/page/game/official/jcxyft.vue'),
				},
				{
					path: 'jc3xyft', //三分飞艇
					component: () => import('@/page/game/official/jc3xyft.vue'),
				},
			]
		},
		{
			path: '/user',
			component: () => import('@/page/layout/UserDefault.vue'),
			children: [
				{
					path: '',
					component: () => import('@/page/user/index.vue'),
				},
				{
					path: 'safe',
					//name: '安全設置',
					component: () => import('@/page/user/safe.vue'),
				},
				{
					path: 'recharge',
					//name: '在線充值',
					component: () => import('@/page/user/recharge.vue'),
				},
				{
					path: 'withdraw',
					//name: '在線提款',
					component: () => import('@/page/user/withdraw.vue'),
				},
				{
					path: 'coin',
					//name: '資金紀錄',
					component: () => import('@/page/user/coin.vue'),
				},
				{
					path: 'betlog',
					//name: '彩票注單',
					component: () => import('@/page/user/user-center-betlog.vue'),
				},
				{
					path: 'ebetlog',
					//name: '電子注單',
					component: () => import('@/page/user/user-center-ebetlog.vue'),
				},
				{
					path: 'withdrawLog',
					//name: '提款記錄',
					component: () => import('@/page/user/withdraw-log.vue'),
				},
				{
					path: 'rechargeLog',
					//name: '充值記錄',
					component: () => import('@/page/user/recharge-log.vue'),
				},
				{
					path: 'mail',
					//name: '站內消息',
					component: () => import('@/page/user/mail.vue'),
				},
			]
		},
		{
			path: '/agent/cash',
			component: () => import('@/page/layout/AgentDefault.vue'),
			children: [
				{
					path: 'info',
					//name: '代理總覽',
					component: () => import('@/page/agent/cash/info.vue'),
				},
				{
					path: 'qr',
					//name: '邀請碼設置',
					component: () => import('@/page/agent/cash/qr.vue'),
				},
				{
					path: 'list',
					//name: '會員列表',
					component: () => import('@/page/agent/cash/list.vue'),
				},
				{
					path: 'index',
					//name: '團隊統計',
					component: () => import('@/page/agent/cash/index.vue'),
				},
			]
		},
		{
			path: '/agent/credit',
			component: () => import('@/page/layout/AgentDefault.vue'),
			children: [
				{
					path: 'index',
					//name: '團隊統計',
					component: () => import('@/page/agent/credit/index.vue'),
				},
				{
					path: 'qr',
					//name: '邀請碼設置',
					component: () => import('@/page/agent/credit/qr.vue'),
				},
				{
					path: 'Memberlist',
					//name: '會員列表',
					component: () => import('@/page/agent/credit/member_list.vue'),
				},
				{
					path: 'AgentList',
					//name: '代理列表',
					component: () => import('@/page/agent/credit/agent_list.vue'),
				},
			]
		},
	]
})
router.beforeEach((to, from, next) => {
	var regex_user = /^\/user\//i;
	var regex_agent = /^\/agent/i;
	if (to.path.search(regex_user) === -1 && to.path.search(regex_agent) === -1) {
		next();
	}else
	{
		if(router.app.$options.store.getters.user.username=='' || router.app.$options.store.getters.user.forTest==1)
		{
			return next({path: "/"});
		}
		next();
	}
})
export default router;
