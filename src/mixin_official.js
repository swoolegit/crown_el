import { apiPostBet1,apiPostBetMax4D } from '@/api.js'
export const OfficialMixin = {
    data() {
        return {
            cal:{
                combine:function(arr, num) {
                    var r = [];
                    (function f(t, a, n) {
                        if (n == 0) return r.push(t);
                        for (var i=0,l=a.length;i<=l-n;i++) {
                            f(t.concat(a[i]), a.slice(i + 1), n - 1);
                        }
                    })([], arr, num);
                    return r;
                },
                getSSCdxds:function(num)
                {
                    var str="";
                    if(num%2 != 0)
                    {
                        str+="d";
                    }
                    if(num%2 == 0)
                    {
                        str+="s";
                    }
                    return str;
                },
                // 获取两个数组中相同元素的个数
                Sames: function(a, b) {
                    var num = 0;
                    for (var i=0;i<a.length;i++) {
                        var zt = 0;
                        for (var j=0;j<b.length;j++) {
                            if (a[i] - b[j] == 0) zt = 1;
                        }
                        if (zt==1) num += 1;
                    }
                    return num;
                },
                // 排列算法
                permutation: function(arr, num) {
                    var r=[];
                    (function f(t, a, n) {
                        if (n == 0) return r.push(t);
                        for (var i=0,l=a.length;i<l;i++){
                            f(t.concat(a[i]), a.slice(0,i).concat(a.slice(i+1)), n-1);
                        }
                    })([], arr, num);
                    return r;
                },
                // 校验号码是否重复
                isRepeat: function(arr) {
                    var hash = {};
                    for (var i in arr) {
                        if (hash[arr[i]]) return true;
                        hash[arr[i]] = true;
                    }
                    return false;
                },
                // 字符串切分
                strCut: function(str, len) {
                    var strlen = str.length;
                    if (strlen == 0) return false;
                    var j = Math.ceil(strlen / len);
                    var arr = Array();
                    for (var i=0;i<j;i++) arr[i] = str.substr(i*len, len);
                    return arr;
                },
                // 枚举排除对子、豹子
                DescartesAlgorithm: (function fn() {
                    var i,j,a=[],b=[],c=[];
                    if(arguments.length==1){
                        if(!Array.isArray(arguments[0])){
                            return [arguments[0]];
                        }else{
                            return arguments[0];
                        }
                    }
                    if(arguments.length>2){
                        for(i=0;i<arguments.length-1;i++) a[i]=arguments[i];
                        b=arguments[i];
                        return fn(fn.apply(null, a), b);
                    }
                    if(Array.isArray(arguments[0])){
                        a=arguments[0];
                    }else{
                        a=[arguments[0]];
                    }
                    if(Array.isArray(arguments[1])){
                        b=arguments[1];
                    }else{
                        b=[arguments[1]];
                    }
                    for(i=0; i<a.length; i++){
                        for(j=0; j<b.length; j++){
                            if(Array.isArray(a[i])){
                                c.push(a[i].concat(b[j]));
                            }else{
                                c.push([a[i],b[j]]);
                            }
                        }
                    }
                    return c;
                }),
            },
        };
    },
    methods: {
        click_play_menu(tab, event)
        {
			this.selectMenu = tab.name;
			this.options.gplayedId = tab.name;
        },
        get_play_menu(ref,id,path)
        {
            let target=this.$refs[ref][0];
            let str=target.className;
            if(this.targetPMenu)
            {
				for(var key in this.targetPMenu)
				{
					this.$refs[this.targetPMenu[key]][0].className = this.$refs[this.targetPMenu[key]][0].className.replace(" checked", "");
				}
            }else
			{
				this.$refs['p-'+this.options.gplayedId][0].className = this.$refs['p-'+this.options.gplayedId][0].className.replace(" checked", "");
			}
			this.targetMenu = target;
            this.targetPMenu=[];
            this.targetPMenu[this.selectMenu] = ref;
            target.className = str+" checked";
			this.options.playedId = id;
			this.default_view = require('@/components/game/official/'+path+'/'+this.mapByPlay[id].tpl+'.vue').default;
			this.RestChooseBet();
            return;
        },
        menu_plays(gid)
        {
            if(this.selectMenu == '')
            {
                return [];
            }
            let Data = [];
            let checked=false;
            for(var key in this.mapByGroupToPlay[gid])
            {
                if(this.targetMenu==null)
                {
                    if(this.mapByGroupToPlay[gid][key].id==this.options.playedId)
                    {
                        checked=true;
                    }else
					{
						checked=false;
					}
                }
                Data.push({
                    "id":this.mapByGroupToPlay[gid][key].id,
                    "name":this.mapByGroupToPlay[gid][key].name,
                    "sort":this.mapByGroupToPlay[gid][key].sort,
                    "checked":checked,
                });
            }
            Data = Data.sort(function (a, b) {
                return a.sort > b.sort ? 1 : -1;
            });
            return Data;
        },
        addBetWeiShu_OF(e)
        {
            let DataFlag=this[this.rule](e,null);
            this.$store.commit('AddbetData',DataFlag.Flag);
            if(DataFlag.FlagOF.length > 0)
            {
                this.$store.commit('AddbetFlag',DataFlag.FlagOF);
            }
            return;
        },
        addBet_OF(e)
        {
            var str=e.target.className;
            var target=e.target;
            if(target.nodeName=="LI")
            {
                target=e.target.childNodes[0];
                str=target.className;
            }
            if(str.indexOf("balls")==-1)
            {
                return;
            }
            //console.log(target);
            //console.log(this.betData);
            let DataFlag=this[this.rule](e,target);
            this.$store.commit('AddbetData',DataFlag.Flag);
            if(DataFlag.FlagOF.length > 0)
            {
                this.$store.commit('AddbetFlag',DataFlag.FlagOF);
                if(str.indexOf("addOF")==-1)
                {
                    target.className=str+" addOF";
                }
            }
            return;
        },
        SSC_ZX(e,target) {
            var codeLen =this.len;
            var Data=[];
            var FlagOF=[];
            var Flag={};
            let dLen = 0;
            let tLen = 0;
            let anum = 0;
            let c,d,wlen,clen,WeiShu=0;
            let delimiter = this.delimiter;
            var temp = this.getData_Flag_OF(target);
            Data = temp.Data;
            FlagOF = temp.FlagOF;
            if(Data[0])
            {
                if(Data[0].length < this.ruleInfo[0].maxball)
                {
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'',
                        "betCount":0,
                    };
                }
            }
            if(Data[1])
            {
                if(Data[1].length < this.ruleInfo[1].maxball)
                {
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'',
                        "betCount":0,
                    };
                }
            }
            switch(true)
            {
                case this.actionRule=='DesAlgorSelect':
                    var temp = '';
                    var check = false;
                    for(var i = 0;i < Data.length;i++)
                    {
                        if(Data[i].length==0)
                        {
                            check = true;
                            break;
                        }
                        temp += ","+Data[i].join(delimiter);
                    }
                    if(check)
                    {
                        Flag={
                            "playedId":this.playedId,
                            "data":Data,
                            "betData":'',
                            "betCount":0,
                        };
                        break;
                    }
                    temp = temp.substr(1,temp.length);
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":temp,
                        "betCount":this.cal.DescartesAlgorithm.apply(null, temp.split(",").map(function(v){return v.split(delimiter)})).map(function(v){ return v.join(','); })
                        .filter(function(v){ return (!this.cal.isRepeat(v.split(","))) },this)
                        .length,
                    };
                    break;
                case this.actionRule=='R3_6':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'',
                        "betCount":0,
                    };
                    wlen = this.WeiShuList.length;
                    if(wlen < codeLen)
                    {
                        break;
                    }
                    clen = this.cal.combine(this.WeiShuList,3);
                    Flag = [];
                    for(var i=0;i<clen.length;i++)
                    {
                        WeiShu = 0;
                        for(var j=0;j<clen[i].length;j++)
                        {
                            WeiShu += clen[i][j];
                        }
                        Flag.push(
                            {
                                "playedId":this.playedId,
                                "data":Data,
                                "betData":Data[0].join(''),
                                "betCount":this.cal.combine(Data[0], this.numberLen).length,
                                "WeiShu":WeiShu,
                            }
                        );
                    }
                    break;
                case this.actionRule=='R3_3':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'',
                        "betCount":0,
                    };
                    wlen = this.WeiShuList.length;
                    if(wlen < codeLen)
                    {
                        break;
                    }
                    clen = this.cal.combine(this.WeiShuList,3);
                    Flag = [];
                    for(var i=0;i<clen.length;i++)
                    {
                        WeiShu = 0;
                        for(var j=0;j<clen[i].length;j++)
                        {
                            WeiShu += clen[i][j];
                        }
                        Flag.push(
                            {
                                "playedId":this.playedId,
                                "data":Data,
                                "betData":Data[0].join(''),
                                "betCount":this.cal.permutation(Data[0], this.numberLen).length,
                                "WeiShu":WeiShu,
                            }
                        );
                    }
                    break;
                case this.actionRule=='Q3_2X_BDW':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(' '),
                        "betCount":this.cal.combine(Data[0], codeLen).length,
                    };
                    break;
                case this.actionRule=='Z3_6' || this.actionRule=='Q3R1BDD' || this.actionRule=='Z2':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(''),
                        "betCount":this.cal.combine(Data[0], codeLen).length,
                    };
                    break;
                case this.actionRule=='Z2R' || this.actionRule=='R3R1BDD':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'',
                        "betCount":0,
                    };
                    wlen = this.WeiShuList.length;
                    if(wlen < codeLen)
                    {
                        break;
                    }
                    for(var i=0;i<this.WeiShuList.length;i++)
                    {
                        WeiShu += this.WeiShuList[i];
                    }
                    clen = this.cal.combine(Data[0], this.numberLen).length;
                    if (wlen == 3 && wlen > codeLen) clen = clen * 3;
                    if (wlen == 4 && wlen > codeLen) clen = clen * 6;
                    if (wlen == 5 && wlen > codeLen) clen = clen * 10;
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(''),
                        "betCount":clen,
                        "WeiShu":WeiShu,
                    };
                    break;
                case this.actionRule=='Z3_3' || this.actionRule=='Z2':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(''),
                        "betCount":this.cal.permutation(Data[0], codeLen).length,
                    };
                    break;
                case this.actionRule=='Z5_120':
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(delimiter),
                        "betCount":this.cal.combine(Data[0], codeLen).length,
                    };
                    break;
                case this.actionRule=='K3_2TDX':
                    if(target!=null)
                    {
                        if(target.dataset.row=="0")
                        {
                            // 處理同號
                            if(Data[0])
                            {
                                var check = target.dataset.value.substr(0,1);
                                // console.log(check);
                                // 處理不同號
                                if(Data[1].length > 0)
                                {
                                    temp = Data[1].indexOf(check);
                                    if(temp > -1)
                                    {
                                        this.delFlag_OF(this.playedId,"1",check);
                                        Data[1].splice(temp,1);
                                    }
                                }
                            }
                        }
                        if(target.dataset.row=="1")
                        {
                            if(Data[0].length > 0)
                            {
                                var check = target.dataset.value+target.dataset.value;
                                temp = Data[0].indexOf(check);
                                if(temp > -1)
                                {
                                    this.delFlag_OF(this.playedId,"0",check);
                                    Data[0].splice(temp,1);
                                }
                            }
                        }
                    }
                    if(Data[1].length < 1 || Data[0].length < 1)
                    {
                        Flag={
                            "playedId":this.playedId,
                            "data":Data,
                            "betData":'',
                            "betCount":0,
                        };
                        break;
                    }
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join(delimiter) + ',' + Data[1].join(delimiter),
                        "betCount":Data[0].length * Data[1].length,
                    };
                    break;
                case this.actionRule=='11x5_ZX':
                    if(target!=null)
                    {
                        if(target.dataset.row=="0")
                        {
                            // 處理膽碼
                            if(Data[0])
                            {

                                this.delFlag_OF(this.playedId,"0");
                                Data[0].splice(0,Data[0].length);
                                Data[0].push(target.dataset.value);
                                FlagOF.splice(0,FlagOF.length);

                                FlagOF.push({
                                    "playedId":this.playedId,
                                    "row":target.dataset.row,
                                    "value":target.dataset.value,
                                    "target":target,
                                });
                                target.className=target.className+" addOF";
                                // 處理托碼
                                if(Data[1].length > 0)
                                {
                                    temp = Data[1].indexOf(target.dataset.value);
                                    if(temp > -1)
                                    {
                                        this.delFlag_OF(this.playedId,"1",target.dataset.value);
                                        Data[1].splice(temp,1);
                                    }
                                }
                            }
                        }
                        if(target.dataset.row=="1")
                        {
                            if(Data[0].length > 0)
                            {
                                temp = Data[0].indexOf(target.dataset.value);
                                if(temp > -1)
                                {
                                    FlagOF.splice(0,FlagOF.length);
                                    Data[1].splice(Data[1].indexOf(target.dataset.value),1);
                                }
                            }
                        }
                    }
                    if(Data[1].length < 1 || Data[0].length < 1)
                    {
                        Flag={
                            "playedId":this.playedId,
                            "data":Data,
                            "betData":'',
                            "betCount":0,
                        };
                        break;
                    }
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":'(' + Data[0].join(' ') + ')' + Data[1].join(' '),
                        "betCount":this.cal.combine(Data[1], codeLen - Data[0].length).length,
                    };
                    break;
                case this.actionRule=='Z5_60':
                    dLen = Data[0].length;
                    tLen = Data[1].length;
                    let sele_count = new Array('0','0','0','1','4','10','20','35','56','84');
                    let num = this.cal.Sames(Data[0], Data[1]);
                    if (tLen - 1 >= 0) {
                        c = tLen - 1;
                    } else {
                        c = 0;
                    }
                    if (num - 1 >= 0) {
                        if (dLen - num == 0) {
                            anum = sele_count[c] * dLen;
                        } else if (dLen - num > 0) {
                            anum = sele_count[tLen] * (dLen - num) + sele_count[c] * num;
                        }
                    } else {
                        anum = sele_count[tLen] * dLen;
                    }
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join('') + ',' + Data[1].join(''),
                        "betCount":anum,
                    };
                    break;
                case this.actionRule=='Z5_30':
                    dLen = Data[0].length;
                    tLen = Data[1].length;
                    for (var i=0;i<dLen-1;i++) {
                        d = i + 1;
                        for (var j=d;j<dLen;j++) {
                            for (c=0;c<tLen;c++) {
                                if(Data[0][i] - Data[1][c] != 0 && Data[0][j] - Data[1][c] != 0) anum = anum + 1;
                            }
                        }
                    }
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join('') + ',' + Data[1].join(''),
                        "betCount":anum,
                    };
                    break;
                case this.actionRule=='Z5_20':
                    dLen = Data[0].length;
                    tLen = Data[1].length;
					for (var i=0;i<tLen-1;i++) {
						d = i + 1;
						for (var j=d;j<tLen;j++) {
							for (c=0;c<dLen;c++) {
								if (Data[1][i] - Data[0][c] != 0 && Data[1][j] - Data[0][c] != 0) anum = anum + 1;
							}
						}
					}
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join('') + ',' + Data[1].join(''),
                        "betCount":anum,
                    };
                    break;
                case this.actionRule=='Z5_10' || this.actionRule=='Z5_5':
                    dLen = Data[0].length;
                    tLen = Data[1].length;
					for (var i=0;i<dLen;i++) {
						for (var j=0;j<tLen;j++) {
							if (Data[0][i] - Data[1][j] != 0) anum = anum + 1;
						}
					}
                    Flag={
                        "playedId":this.playedId,
                        "data":Data,
                        "betData":Data[0].join('') + ',' + Data[1].join(''),
                        "betCount":anum,
                    };
                    break;
            }
            return{
                "Flag":Flag,
                "FlagOF":FlagOF,
            };
        },
        SSCInputRxDs()
        {
            var codeLen = this.numberLen;
            var codes = [];
            var weiShu = [];
            var weiShuSum = 0;
            var str = this.value.replace(/[^\d]/g, '');
            if (str.length && str.length % codeLen == 0) {
                if(/[^\d]/.test(str))
                {
                    this.error = true;
                    this.errorMsg = '不能有数字以外的字符';
                    return;
                }
                codes = codes.concat(str.match(new RegExp('\\d{'+codeLen+'}', 'g')));
            } else {
                this.error = true;
                this.errorMsg = '您选择或输入的数字错误';
                return;
            }
            if(this.WeiShuList.length < codeLen)
            {
                this.error = true;
                this.errorMsg = '请至少选择'+codeLen+'个位置';
                return;
            }else
            {
                this.WeiShuList.forEach(function(v, i) {
                    weiShu.push(i);
                    weiShuSum += v;
                })
            }
            var tcodes = codes.join('');
            var temp = ['-','-','-','-','-'];
            var weitype = this.len;
            var tlen = tcodes.length / weitype;
            // 计算是否有重复输入
            var arTemp=[];
            for (var i=0;i<tlen;i++) {
                arTemp.push(tcodes.substr(i*weitype,weitype));
            }
            var code = [];
            var hsTemp = {};
            var reTemp = [];
            for (var i=0;i<arTemp.length;i++) {
                if (!hsTemp[arTemp[i]]) {
                    hsTemp[arTemp[i]] = true;
                    var strTemp = arTemp[i].split('');
                    for (var kk=0;kk<strTemp.length;kk++) {
                        code.splice(code.length, 0, strTemp[kk]);
                    }
                } else {
                    reTemp.push(arTemp[i]);
                }
            }
            if(reTemp.length > 0)
            {
                this.error = true;
                this.errorMsg = '存在输入重复的数据：'+reTemp.join(',');
                return;
            }
            var k = 0;
            var g = 0;
            switch(true)
            {
                case weitype == 2 && this.actionRule == '2X_R2':
                    code.forEach(function(v0, i0) {
                        if (tlen <= i0) return;
                        weiShu.forEach(function(v1, i1) {
                            weiShu.forEach(function(v2, i2) {
                                if(v1 != v2 && v1 < v2) {
                                    var temp1 = [];
                                    temp1.push(v1);
                                    temp1.push(v2);
                                    k = i0 * weitype;
                                    temp1.forEach(function(v3, i3) {
                                        temp[v3] = code[k];
                                        k++;
                                    });
                                    codes[g] = temp;
                                    g++;
                                    temp = ['-','-','-','-','-'];
                                }
                            });
                        });
                    });
                    break;
                    case weitype == 3 && this.actionRule == '3X_R3':
                        code.forEach(function(v0, i0) {
                            if (tlen <= i0) return;
                            weiShu.forEach(function(v1, i1) {
                                weiShu.forEach(function(v2, i2) {
                                    if (v1 != v2 && v1 < v2) {
                                        weiShu.forEach(function(v3, i3) {
                                            if (v2 != v3 && v2 < v3) {
                                                var temp1 = [];
                                                temp1.push(v1);
                                                temp1.push(v2);
                                                temp1.push(v3);
                                                k = i0 * weitype;
                                                temp1.forEach(function(v4, i4) {
                                                    temp[v4] = code[k];
                                                    k++;
                                                });
                                                codes[g] = temp;
                                                g++;
                                                temp = ['-','-','-','-','-'];
                                            }
                                        });
                                    }
                                });
                            });
                        });
                        break;
                    case weitype == 4 && this.actionRule == '4X_R4':
                        code.forEach(function(v0, i0) {
                            if (tlen <= i0) return;
                            weiShu.forEach(function(v1, i1) {
                                weiShu.forEach(function(v2, i2) {
                                    if (v1 != v2 && v1 < v2) {
                                        weiShu.forEach(function(v3, i3) {
                                            if (v2 != v3 && v2 < v3) {
                                                weiShu.forEach(function(v4, i4) {
                                                    if (v3 != v4 && v3 < v4) {
                                                        var temp1 = [];
                                                        temp1.push(v1);
                                                        temp1.push(v2);
                                                        temp1.push(v3);
                                                        temp1.push(v4);
                                                        k = i0 * weitype;
                                                        temp1.forEach(function(v5, i5) {
                                                            temp[v5] = code[k];
                                                            k++;
                                                        });
                                                        codes[g] = temp;
                                                        g++;
                                                        temp = ['-','-','-','-','-'];
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                        });
                        break;
            }
            this.error = false;
            this.delBet_OF(this.playedId);
            var Flag={
                "playedId":this.playedId,
                "data":this.value,
                "betData":codes.join('|'),
                "betCount":codes.length,
                "WeiShu":weiShuSum,
            };
            this.$store.commit('AddbetData',Flag);
        },
        tz11x5Inputrxds: function() {
            var codeLen = this.len * 2;
            var codes = [];
            var str = this.value.replace(/[^\d]/g, '');
            var str2 = str;
            var str2 = this.cal.strCut(str2, 2);
            var info = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11'];
            //if (lottery.isRepeat(str2)) throw('您输入的数字有重复，请重新输入');
            if (str.length < codeLen)
            {
                this.error = true;
                this.errorMsg = '至少输入'+this.len+'位数字';
                return;
            }
            if (str.length && str.length % codeLen == 0) {
                if(/[^\d]/.test(str)) //throw('投注有错，不能有数字以外的字符');
                {
                    this.error = true;
                    this.errorMsg = '不能有数字以外的字符';
                    return;
                }
                for (var j=0;j<str2.length;j++) {
                    if (info.indexOf(str2[j]) == -1) //throw('您输入的数字有误，请重新输入');
                    {
                        this.error = true;
                        this.errorMsg = '您输入的数字有误，请重新输入';
                        return;
                    }
                }
                codes = codes.concat(str.match(new RegExp('\\d{'+codeLen+'}', 'g')));
            } else {
                len=0;
            }
            this.error = false;
            this.delBet_OF(this.playedId);
            var Flag={
                "playedId":this.playedId,
                "data":this.value,
                "betData":codes.join('|'),
                "betCount":codes.length,
            };
            this.$store.commit('AddbetData',Flag);
        },
        tz11x5WeiInput: function() {
            var codeLen = this.len;
            var codes=[];
            var weiShu=[0,1,2,3,4];
            var weiShuSum = 0;
            var ncode;
            var str = this.value.replace(/[^\d]/g, '');
            if(this.WeiShuList.length < codeLen)
            {
                this.error = true;
                this.errorMsg = '请至少选择'+codeLen+'个位置';
                return;
            }else
            {
                this.WeiShuList.forEach(function(v, i) {
                    //weiShu2.push(i);
                    weiShuSum += v;
                    switch(true)
                    {
                        case v == 16:
                            var index = weiShu.indexOf(0);
                            weiShu.splice(index,1);
                            break;
                        case v == 8:
                            var index = weiShu.indexOf(1);
                            weiShu.splice(index,1);
                            break;
                        case v == 4:
                            var index = weiShu.indexOf(2);
                            weiShu.splice(index,1);
                            break;
                        case v == 2:
                            var index = weiShu.indexOf(3);
                            weiShu.splice(index,1);
                            break;
                        case v == 1:
                            var index = weiShu.indexOf(4);
                            weiShu.splice(index,1);
                            break;
                    }
                })
            }
            codeLen *= 2;
            if (str.length && str.length % codeLen == 0) {
                if(/[^\d]/.test(str))
                {
                    this.error = true;
                    this.errorMsg = '不能有数字以外的字符';
                    return;
                }
                codes = codes.concat(str.match(new RegExp('\\d{'+codeLen+'}', 'g')));
            } else {
                this.error = true;
                this.errorMsg = '您选择或输入的数字错误';
                return;
            }

            codes = codes.map(function(code) {
                code = code.split("");
                ncode = "";
                code.forEach(function(v,i) {
                    if (i % 2==0 && ncode) {
                        ncode += "," + v;
                    } else {
                        ncode += v;
                    }
                });
                ncode = ncode.split(',');
                weiShu.forEach(function(v,i) {
                    ncode.splice(v, 0, '-');
                });
                return ncode;
            });
            this.error = false;
            this.delBet_OF(this.playedId);
            var Flag={
                "playedId":this.playedId,
                "data":this.value,
                "betData":codes.join('|'),
                "betCount":codes.length,
                "WeiShu":weiShuSum,
            };
            this.$store.commit('AddbetData',Flag);
        },
        tz11x5Input() {
            var codeLen = this.len*2;
            var codes = [];
            var ncode;
            var str = this.value.replace(/[^\d]/g, '');
            if (str.length && str.length % codeLen == 0) {
                if(/[^\d]/.test(str))
                {
                    this.error = true;
                    this.errorMsg = '不能有数字以外的字符';
                    return;
                }
                codes = codes.concat(str.match(new RegExp('\\d{'+codeLen+'}', 'g')));
            } else {
                this.error = true;
                this.errorMsg = '您选择或输入的数字错误';
                return;
            }
            codes = codes.map(function(code) {
                code = code.split("");
                ncode = "";
                code.forEach(function(v, i) {
                    if (i % 2==0 && ncode) {
                        ncode += ',' + v;
                    } else {
                        ncode += v;
                    }
                });
                return ncode;
            });
            this.error = false;
            this.delBet_OF(this.playedId);
            var Flag={
                "playedId":this.playedId,
                "data":this.value,
                "betData":codes.join('|'),
                "betCount":codes.length,
            };
            this.$store.commit('AddbetData',Flag);
        },
        SSCInputDs() {
            var codeLen = this.len;
            var codes = [];
            var str = this.value.replace(/[^\d]/g, '');
            if (str.length && str.length % codeLen == 0) {
                if(/[^\d]/.test(str))
                {
                    this.error = true;
                    this.errorMsg = '不能有数字以外的字符';
                    return;
                }
                codes = codes.concat(str.match(new RegExp('\\d{'+codeLen+'}', 'g')));
            } else {
                this.error = true;
                this.errorMsg = '您选择或输入的数字错误';
                return;
            }
            codes = codes.map(function(code) {
                return code.split('').join(',')
            });
            this.error = false;
            this.delBet_OF(this.playedId);
            var Flag={
                "playedId":this.playedId,
                "data":this.value,
                "betData":codes.join('|'),
                "betCount":codes.length,
            };
            this.$store.commit('AddbetData',Flag);
        },
        SSCInputDsZX() {
            var codeList = this.value;
            var played = this.played;
            var z3 = [];
            var z6 = [];
            var z = [];
            var n = 0;
            var o = {"前":[16,17],"中":[289,290],"后":[19,20],"任选":[22,23],"混":[59,60]};
            if (played == '任选' && this.WeiShuList.length < 3)
            {
                this.error = true;
                this.errorMsg = '请至少选择3个位置';
                return;
            }
            codeList = codeList.replace(/[^\d]/gm, '');
            if (codeList.length == 0){
                this.error = true;
                this.errorMsg = '请输入您投注的号码';
                return;
            };
            if (codeList.length % 3){
                this.error = true;
                this.errorMsg = '您输入的号码个数不符合玩法规则';
                return;
            };
            codeList.replace(/[^\d]/gm, '').match(/\d{3}/g).forEach(function(code) {
                var str = code.toString();
                var ff = 0;
                var sum1 = '';
                var xx = [];
                var yy = 0;
                var i;
                xx[0] = parseInt(str.substr(0,1));
                xx[1] = parseInt(str.substr(1,1));
                xx[2] = parseInt(str.substr(2,1));
                for (i=1;i<3;i++) {
                    if (xx[i] > xx[0]) {
                        yy=xx[0];
                        xx[0]=xx[i];
                        xx[i]=yy;
                    }
                }
                for (i=2;i<3;i++) {
                    if (xx[i] > xx[1]) {
                        yy=xx[1];
                        xx[1]=xx[i];
                        xx[i]=yy;
                    }
                }
                sum1 = xx[0] + ',' + xx[1] + ',' + xx[2];
                z.push(sum1);
                if (n >= 1) {
                    for (var a=0;a<n;a++) {
                        if(z[n]==z[a]) ff=1;
                    }
                }

                if (ff==0) {
                    var reg = /(\d)(.*)\1/;
                    if (/(\d)\1{2}/.test(code)) {
                        this.error = true;
                        this.errorMsg = '组选不能为豹子';
                        return;
                    } else if (reg.test(code)) {
                        //z3.push(code);
                        z3.push(str.substr(0,1)+str.substr(1,1)+str.substr(2,1));
                    } else {
                        //z6.push(code);
                        z6.push(str.substr(0,1)+str.substr(1,1)+str.substr(2,1));
                    }
                }
                n = n + 1;
            },this);
            if(z3.length || z6.length)
            {
                this.error = false;
            }
            this.delBet_OF(o[played][0]);
            this.delBet_OF(o[played][1]);
            if(played == '任选')
            {
                var weiShu = 0;
                var weiShusel = [];
                var weizhiAr = [];
                this.WeiShuList.forEach(function(v, i) {
                    weiShusel.push(i);
                })
                var weishuzhi = [16,8,4,2,1];
                weiShusel.forEach(function(v1, i1) {
                    weiShusel.forEach(function(v2, i2) {
                        if (v1 != v2 && v1 < v2) {
                            weiShusel.forEach(function(v3, i3) {
                                if (v2 != v3 && v2 < v3) {
                                    weiShu |= parseInt(weishuzhi[v1]);
                                    weiShu |= parseInt(weishuzhi[v2]);
                                    weiShu |= parseInt(weishuzhi[v3]);
                                    weizhiAr[weizhiAr.length] = weiShu;
                                    weiShu = 0;
                                }
                            });
                        }
                    });
                });
            }
            if (z3.length) {
                if(played == '任选')
                {
                    for (var i=0;i<weizhiAr.length;i++) {
                        this.$store.commit('AddbetData',{
                            "playedId":o[played][0],
                            "data":z3,
                            "betData":z3.join(','),
                            "betCount":z3.length,
                            "WeiShu":weizhiAr[i]
                        });
                    }
                }else
                {
                    this.$store.commit('AddbetData',{
                        "playedId":o[played][0],
                        "data":z3,
                        "betData":z3.join(','),
                        "betCount":z3.length,
                    });
                }
            }
            if (z6.length) {
                if(played == '任选')
                {
                    for (var i=0;i<weizhiAr.length;i++) {
                        this.$store.commit('AddbetData',{
                            "playedId":o[played][1],
                            "data":z6,
                            "betData":z6.join(','),
                            "betCount":z6.length,
                            "WeiShu":weizhiAr[i]
                        });
                    }
                }else
                {
                    this.$store.commit('AddbetData',{
                        "playedId":o[played][1],
                        "data":z6,
                        "betData":z6.join(','),
                        "betCount":z6.length,
                    });
                }
            }
        },
        setAction_OF(e)
        {
            var target=e.target;
            var Data=[];
            var row = target.dataset.row;
            var actionData=[];
            var numsData=this.ruleInfo[parseInt(row)].nums;
            this.clearRow(this.playedId,row);
            switch(target.dataset.rule)
            {
                case 'd':
                    for(var i = 0;i<numsData.length;i++)
                    {
                        if(parseInt(numsData[i])%2 !=0)
                        {
                            actionData.push(numsData[i]);
                        }
                    }
                    break;
                case 's':
                    for(var i = 0;i<numsData.length;i++)
                    {
                        if(parseInt(numsData[i])%2 ==0)
                        {
                            actionData.push(numsData[i]);
                        }
                    }
                    break;
                case 'max':
                        for(var i = 0;i<numsData.length;i++)
                        {
                            if(parseInt(numsData[i])>=this.ruleInfo[parseInt(row)].max)
                            {
                                actionData.push(numsData[i]);
                            }
                        }
                        break;
                case 'min':
                        for(var i = 0;i<numsData.length;i++)
                        {
                            if(parseInt(numsData[i])<=this.ruleInfo[parseInt(row)].min)
                            {
                                actionData.push(numsData[i]);
                            }
                        }
                        break;
                case 'a':
                        for(var i = 0;i<numsData.length;i++)
                        {
                            actionData.push(numsData[i]);
                        }
                        break;
                case 'c':
                        break;
            }
            for(var i=0;i<this.ruleInfo.length;i++)
            {
                if(i==parseInt(row))
                {
                    Data[i]=actionData;
                    if(actionData.length==0)
                    {
                        let DataFlag=this[this.rule](e,null);
                        if(Object.keys(DataFlag.Flag).length > 0)
                        {
                            this.$store.commit('AddbetData',DataFlag.Flag);
                        }
                    }else
                    {
                        for(var j = 0;j<actionData.length;j++)
                        {
                            let actionTarget=this.$refs[row+'-'+actionData[j]][0];
                            let str=actionTarget.className;
                            let DataFlag=this[this.rule](e,actionTarget,false);
                            this.$store.commit('AddbetData',DataFlag.Flag);
                            if(Object.keys(DataFlag.FlagOF).length > 0)
                            {
                                this.$store.commit('AddbetFlag',DataFlag.FlagOF);
                                actionTarget.className=str+" addOF";
                            }
                        }
                    }
                }
            }
        },

        randBet_OF()
        {
            let Data=[];
            let WeiShuData = [];
            let numData = [];
            let betCount=1;
            let Flag={};
            let WeiShu=0;
            let index,num,betData,WeiShuList;
            let delimiter = this.$parent.delimiter;
            if(this.$parent.$refs['no-rand'])
            {
                return;
            }
            this.delBet_OF(this.$parent.playedId);
            this.delFlagAll_OF(this.$parent.playedId);
            for(let i = 0 ; i < this.ruleInfo[0].nums.length ; i++)
            {
                numData.push(this.ruleInfo[0].nums[i]);
            }
            for(let i = 0 ; i < this.ruleInfo.length ; i++)
            {
                WeiShuData.push(i);
            }
            switch(true)
            {
                case this.$parent.actionRule=='11x5_ZX':
                    Data[0]=[]
                    Data[1]=[]
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[0].push(this.setRandNum_OF("0",num));
                    numData.splice(index,1);
                    for(let i = 0 ; i < this.$parent.len-1 ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[1].push(this.setRandNum_OF("1",num));
                        numData.splice(index,1);
                    }
                    betData = "("+Data[0]+")"+Data[1].join(delimiter);
                    break;
                case this.$parent.actionRule=='DesAlgorSelect':
                    for(let i = 0 ; i < this.ruleInfo.length ; i++)
                    {
                        index = Math.floor((Math.random() * (this.ruleInfo[i].nums.length )));
                        if(i == 0)
                        {
                            Data[i]=[this.setRandNum_OF(i,this.ruleInfo[i].nums[index])];
                            continue;
                        }
                        var check = false;
                        for(var j = 0;j < Data.length;j++)
                        {
                            if(Data[j]==this.ruleInfo[i].nums[index])
                            {
                                check = true;
                                break;
                            }
                        }
                        if(check)
                        {
                            i = i-1;
                        }else
                        {
                            Data[i]=[this.setRandNum_OF(i,this.ruleInfo[i].nums[index])];
                        }
                    }
                    betData = Data.join(',');
                    break;
                case this.$parent.actionRule=='5X_R2' || this.$parent.actionRule=='5X_R3' || this.$parent.actionRule=='5X_R4':
                    for(let i = 0 ; i < this.ruleInfo.length ; i++)
                    {
                        Data[i]=['-'];
                    }
                    for(let i = 0 ; i < this.$parent.len ; i++)
                    {
                        WeiShu = Math.floor((Math.random() * (WeiShuData.length )));
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[WeiShuData[WeiShu]].splice(0,Data[WeiShuData[WeiShu]].length);
                        Data[WeiShuData[WeiShu]].push(this.setRandNum_OF(WeiShuData[WeiShu],num));
                        WeiShuData.splice(WeiShu,1);
                    }
                    betData = Data.join(',');
                    break;
                case this.$parent.actionRule=='5X_DW':
                    WeiShu = Math.floor((Math.random() * (this.ruleInfo.length )));
                    index = Math.floor((Math.random() * (this.ruleInfo[WeiShu].nums.length )));
                    for(let i = 0 ; i < this.ruleInfo.length ; i++)
                    {
                        Data[i]=[];
                    }
                    num = numData[index];
                    Data[WeiShu]=[[this.setRandNum_OF(WeiShu,num)].join(delimiter)];
                    betData = Data.join(',');
                    break;
                case this.$parent.actionRule=='5X_ZH' :
                    for(let i = 0 ; i < this.ruleInfo.length ; i++)
                    {
                        index = Math.floor((Math.random() * (this.ruleInfo[i].nums.length )));
                        Data[i]=[this.setRandNum_OF(i,this.ruleInfo[i].nums[index])];
                    }
                    betData = Data.join(',');
                    break;
                case this.$parent.actionRule=='Q3_2X_BDW':
                    Data[0]=[]
                    for(let i = 0 ; i < this.$parent.len ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[0].push(this.setRandNum_OF("0",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join(' ');
                    break;
                case this.$parent.actionRule=='Z3_6' || this.$parent.actionRule=='Z2' || this.$parent.actionRule=='Q3R1BDD':
                    Data[0]=[]
                    for(let i = 0 ; i < this.$parent.len ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[0].push(this.setRandNum_OF("0",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join('');
                    break;
                case this.$parent.actionRule=='Z5_120' || this.$parent.actionRule=='Z3_3' || this.$parent.actionRule=='ZXHZ3D':
                    Data[0]=[]
                    for(let i = 0 ; i < this.$parent.len ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[0].push(this.setRandNum_OF("0",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join(delimiter);
                    break;
                case this.$parent.actionRule=='Z2R' || this.$parent.actionRule=='R3R1BDD' || this.$parent.actionRule=='R3_3' || this.$parent.actionRule=='R3_6':
                    WeiShuList=[1,2,4,8,16];
                    Data[0]=[]
                    for(let i = 0 ; i < this.$parent.numberLen ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[0].push(this.setRandNum_OF("0",num));
                        numData.splice(index,1);
                    }
                    this.$parent.WeiShuList.splice(0,this.$parent.WeiShuList.length);
                    for(let i = 0 ; i < this.$parent.len ; i++)
                    {
                        index = Math.floor((Math.random() * (WeiShuList.length )));
                        WeiShu += WeiShuList[index];
                        this.$parent.WeiShuList.push(WeiShuList[index]);
                        WeiShuList.splice(index,1);
                    }
                    Flag.WeiShu=WeiShu;
                    betData = Data[0].join('');
                    break;
                case this.$parent.actionRule=='Z5_60':
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[0]=[this.setRandNum_OF("0",num)];
                    numData.splice(index,1);
                    Data[1]=[];
                    for(let i = 0 ; i < 3 ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[1].push(this.setRandNum_OF("1",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join('') + ',' + Data[1].join('')
                    break;
                case this.$parent.actionRule=='Z5_30':
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[1]=[this.setRandNum_OF("1",num)];
                    numData.splice(index,1);
                    Data[0]=[];
                    for(let i = 0 ; i < 2 ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[0].push(this.setRandNum_OF("0",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join('') + ',' + Data[1].join('')
                    break;
                case this.$parent.actionRule=='Z5_20':
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[0]=[this.setRandNum_OF("0",num)];
                    numData.splice(index,1);
                    Data[1]=[];
                    for(let i = 0 ; i < 2 ; i++)
                    {
                        index = Math.floor((Math.random() * (numData.length )));
                        num = numData[index];
                        Data[1].push(this.setRandNum_OF("1",num));
                        numData.splice(index,1);
                    }
                    betData = Data[0].join('') + ',' + Data[1].join('')
                    break;
                case this.$parent.actionRule=='Z5_10' || this.$parent.actionRule=='Z5_5' :
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[0]=[this.setRandNum_OF("0",num)];
                    numData.splice(index,1);
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[1]=[this.setRandNum_OF("1",num)];
                    numData.splice(index,1);
                    betData = Data[0].join(delimiter) + ',' + Data[1].join(delimiter)
                    break;
                case this.$parent.actionRule=='K3_2TDX':
					let R2_numData = [];
					for(let i = 0 ; i < this.ruleInfo[1].nums.length ; i++)
					{
						R2_numData.push(this.ruleInfo[1].nums[i]);
					}
                    index = Math.floor((Math.random() * (numData.length )));
                    num = numData[index];
                    Data[0]=[this.setRandNum_OF("0",num)];
                    numData.splice(index,1);
                    index = Math.floor((Math.random() * (numData.length )));
                    num = R2_numData[index];
                    Data[1]=[this.setRandNum_OF("1",num)];
                    numData.splice(index,1);
                    betData = Data[0].join(delimiter) + ',' + Data[1].join(delimiter)
                    break;
            }
            Flag.playedId = this.$parent.playedId;
            Flag.data = Data;
            Flag.betData = betData;
            Flag.betCount = betCount;
            this.$store.commit('AddbetData',Flag);
			this.addCart();
            return;
        },
        setRandNum_OF(prow,index)
        {
            let target=this.$parent.$refs[prow+'-'+index][0];
            let str=target.className;
            target.className=str+" addOF";
            let value=target.dataset.value;
            let row=target.dataset.row;
            let FlagOF={};
            FlagOF={
                "playedId":this.$parent.playedId,
                "row":row,
                "value":value,
                "target":target,
            };
            this.$store.commit('AddbetFlag',FlagOF);
            return value;
        },
        SSC5X_ZH(e,target)
        {
            var Data=[];
            var FlagOF=[];
            var betCount=1;
            var betData=[];
            let delimiter = this.delimiter;
            if(this.actionRule=='5X_DW')
            {
                betCount = 0;
            }
            var temp = this.getData_Flag_OF(target);
            Data = temp.Data;
            FlagOF = temp.FlagOF;
            switch(true)
            {
                case this.actionRule=='ZXHZ3D':
                    var sele_count = new Array('1','3','6','10','15','21','28','36','45','55','63','69','73','75','75','73','69','63','55','45','36','28','21','15','10','6','3','1');
                    betCount = 0;
					for (var i=0;i<Data[0].length;i++){
                        var num = Data[0][i];
                        betCount = betCount + parseInt(sele_count[num]);
                        betData.push(Data[0][i]);
					}
                    break;
                case this.actionRule=='5X_DW':
                    for(var i=0;i<Data.length;i++)
                    {
                        betCount += Data[i].length;
                        betData.push(Data[i].join(delimiter));
                    }
                    break;
                case this.actionRule=='5X_ZH':
                    for(var i=0;i<Data.length;i++)
                    {
                        betCount *= Data[i].length;
                        betData.push(Data[i].join(delimiter));
                    }
                    break;
                case this.actionRule=='5X_R2':
                        betCount = 0;
                        for(var i=0; i < Data.length; i++)
                        {
                            for(var j=i+1; j < Data.length; j++)
                            {
                                if(Data[j].length > 0 && Data[i][0]!='-' && Data[j][0]!='-')
                                {
                                    betCount += Data[i].length*Data[j].length;
                                }
                            }
                            if(Data[i].length==0 || Data[i][0]=='-')
                            {
                                betData.push('-');
                            }else
                            {
                                betData.push(Data[i].join(''));
                            }
                        }
                    break;
                case this.actionRule=='5X_R3':
                        betCount = 0;
                        for(var i=0; i < Data.length; i++)
                        {
                            for(var j=i+1; j < Data.length; j++)
                            {
                                for(var k=j+1; k < Data.length; k++)
                                {
                                    if(k < Data.length && Data[k].length > 0 && Data[j].length > 0)
                                    {
                                        if(Data[i][0]!='-' && Data[j][0]!='-' && Data[k][0]!='-')
                                        {
                                            betCount += Data[i].length * Data[k].length * Data[j].length;
                                        }
                                    }
                                }
                            }
                            if(Data[i].length==0 || Data[i][0]=='-')
                            {
                                betData.push('-');
                            }else
                            {
                                betData.push(Data[i].join(''));
                            }
                        }
                    break;
                case this.actionRule=='5X_R4':
                        betCount = 0;
                        for(var i=0; i < Data.length; i++)
                        {
                            for(var j=i+1; j < 3; j++)
                            {
                                for(var k=j+1; k < Data.length; k++)
                                {
                                    for(var l=k+1; l < Data.length; l++)
                                    {
                                        if(l < Data.length && Data[l].length > 0 && Data[k].length > 0 && Data[j].length > 0)
                                        {
                                            if(Data[i][0]!='-' && Data[j][0]!='-' && Data[k][0]!='-' && Data[l][0]!='-')
                                            {
                                                betCount += Data[i].length * Data[l].length * Data[k].length * Data[j].length;
                                            }
                                        }
                                    }
                                }
                            }
                            if(Data[i].length==0 || Data[i][0]=='-')
                            {
                                betData.push('-');
                            }else
                            {
                                betData.push(Data[i].join(''));
                            }
                        }
                    break;
            }

            if(betCount <= 0)
            {
                betData.splice(0,betData.length);
            }
            var Flag={
                "playedId":this.playedId,
                "data":Data,
                "betData":betData.join(','),
                "betCount":betCount,
            };
            return{
                "Flag":Flag,
                "FlagOF":FlagOF,
            };
        },
        getData_Flag_OF(target)
        {
            let row=null;
            let value=null;
            let Data=[];
            let FlagOF=[];
            if(target != null)
            {
                row=target.dataset.row;
                value=target.dataset.value;
            }
            if(this.betData.length > 0)
            {
                for (var i= 0; i< this.betData.length; i++){
                    if(this.betData[i].playedId==this.playedId)
                    {
                        Data = this.betData[i].data;
                        this.delBet_OF(this.playedId);
                        this.delFlag_OF(this.playedId,row,value);
                    }
                }
            }
            for(var i=0;i<this.ruleInfo.length;i++)
            {
                if(i==parseInt(row) && target != null)
                {
                    if(Data[i])
                    {
                        if(Data[i][0]=='-')
                        {
                            Data[i].splice(index,1);
                        }

                        var index = Data[i].indexOf(value);
                        if(index==-1)
                        {
                            Data[i].push(value);
                            FlagOF.push({
                                "playedId":this.playedId,
                                "row":row,
                                "value":value,
                                "target":target,
                            });
                        }else
                        {
                            Data[i].splice(index,1);
                        }
                    }else
                    {
                        Data[i]=[value];
                        FlagOF.push({
                            "playedId":this.playedId,
                            "row":row,
                            "value":value,
                            "target":target,
                        });
                    }
                }else
                {
                    if(!Data[i])
                    {
                        Data[i]=[];
                    }
                }
            }
            return {Data:Data,FlagOF:FlagOF}
        },
        delBet_OF(playedId)
        {
            let Data=[];
            for (var i= 0; i< this.betData.length; i++){
                if(this.betData[i].playedId!=playedId)
                {
                    Data.push(this.betData[i]);
                }
            }
            this.$store.commit('DelbetDataAll');
            this.$store.commit('AddbetData',Data);
        },
        clearRow(playedId,row)
        {
            this.delFlag_OF(playedId,row);
            for (var i= 0; i< this.betData.length; i++){
                if(this.betData[i].playedId==playedId)
                {
                    let Data = this.betData[i].data;
                    Data[row] = [];
                    let Flag={
                        "playedId":playedId,
                        "data":Data,
                        "betData":this.betData[i].betData,
                        "betCount":this.betData[i].betCount,
                    };
                    this.$store.commit('DelbetData',i);
                    this.$store.commit('AddbetData',Flag);
                    break;
                }
            }
        },
        delFlagAll_OF(playedId)
        {
            let Data=[];
            for (var i= 0; i< this.betFlag.length; i++){
                if(this.betFlag[i].playedId==playedId )
                {
                    var target = this.betFlag[i].target;
                    var str=target.className;
                    target.className=str.replace(" addOF", "");
                }else
                {
                    Data.push(this.betFlag[i]);
                }
            }
            this.$store.commit('DelbetFlagAll');
            this.$store.commit('AddbetFlag',Data);
        },
        delFlag_OF(playedId,row='',value='')
        {
            if(value!='' && row!='')
            {
                for (var i= 0; i< this.betFlag.length; i++){
                    if(this.betFlag[i].playedId==playedId && this.betFlag[i].row==row && this.betFlag[i].value==value)
                    {
                        var target = this.betFlag[i].target;
                        var str=target.className;
                        target.className=str.replace(" addOF", "");
                        this.$store.commit('DelbetFlag',i);
                        break;
                    }
                }
            }else if(row!='' && value=='')
            {
                let Data=[];
                for (var i= 0; i< this.betFlag.length; i++){
                    if(this.betFlag[i].playedId==playedId && this.betFlag[i].row==row )
                    {
                        var target = this.betFlag[i].target;
                        var str=target.className;
                        target.className=str.replace(" addOF", "");
                    }else
                    {
                        Data.push(this.betFlag[i]);
                    }
                }
                this.$store.commit('DelbetFlagAll');
                this.$store.commit('AddbetFlag',Data);
                // for (var i= 0; i< Data.length; i++){
                //     this.$store.commit('AddbetFlag',Data[i]);
                // }
            }
        },
        clearNum_OF()
        {
            this.value='';
            var o = {"前":[16,17],"中":[289,290],"后":[19,20],"任选":[22,23],"混":[59,60]};
            this.delBet_OF(this.playedId);
            if(this.actionRule == "Z3_ZX")
            {
                this.delBet_OF(o[this.played][0]);
                this.delBet_OF(o[this.played][1]);
            }
        },
        postZhuihao_OF()
        {
            let betAmount = 0;
            for (var i= 0; i< this.results.length; i++){
                betAmount += this.results[i].amount;
            }
			if(betAmount > this.user.coin)
			{
                this.message("钱包余额不足,请先充值");
                return;
			}
            this.postBet_OF(this.results);
        },
		postBet_OF(zhuihao=null)
		{
            var code=[];
            if(this.CartData.length <=0)
            {
                this.message("请先选择号码");
                return;
            }
            if(this.beiShu == '')
            {
                this.message("请输入倍數");
                return;
            }
            if(this.currentIssue ==null)
            {
                this.message("尚未获取奖期");
                return;
            }
            if(this.user.username =='')
            {
                this.message("请先登录");
                return;
            }
			// var clientTime=(Date.parse( new Date())/1000);
			// if(this.currentIssue.stopTime <= clientTime)
			// {
   //              this.message("封盘中停止下注");
   //              return;
			// }
            let betAmount = 0;
            let weiShu = 0;
            let betCount = 0;
            let beiShu = 0;
            for (var i= 0; i< this.CartData.length; i++){
                betCount += this.CartData[i].betCount;
                betAmount += parseInt(this.CartData[i].actionAmount);
				beiShu = parseInt(this.CartData[i].beiShu);
				if(parseInt(beiShu * betCount * 2) < this.config.min_bet)
				{
					this.message("单笔投注金额不得小于 " + this.config.min_bet);
					return;
				}
                if(this.CartData[i].WeiShu)
                {
                    weiShu = this.CartData[i].WeiShu;
                }else
                {
                    weiShu = 0;
                }
                code.push({
                    actionData:this.CartData[i].betData,
                    playedId:this.CartData[i].playedId,
                    beiShu:beiShu,
                    type:this.options.type_id,
                    weiShu:weiShu,
                });
            }
			if(betAmount > this.user.coin)
			{
                this.message("钱包余额不足,请先充值");
                return;
			}
            let Data = {
                code : code,
                para:{
                    type:this.options.type_id,
                    actionNo:this.currentIssue.actionNo,
                    kjTime:this.currentIssue.actionTime,
                    betType:1
                },
            };
            if(zhuihao)
            {
                Data.zhuihao=zhuihao;
            }
			let loadingInstance = this.$loading(this.loadOptions);
            apiPostBet1(Data)
            .then(response => {
                loadingInstance.close();
                if(response.data.error)
                {
                    this.error_handler(response.data);
                }
                if(response.data.status)
                {
                    this.$store.dispatch('setCoin',response.data.coin);
                    this.message(response.data.msg);
                    this.showme = false;
                }
				this.restBet();
                return;
            }).catch(error => {
				loadingInstance.close();
                console.log(error);
                return;
            });
		},
        postBet_4d(zhuihao=null)
        {
            var code=[];
            if(this.CartData.length <=0)
            {
                this.message("请先选择号码");
                return;
            }
            if(this.beiShu == '')
            {
                this.message("请输入倍數");
                return;
            }
            if(this.currentIssue ==null)
            {
                this.message("尚未获取奖期");
                return;
            }
            if(this.user.username =='')
            {
                this.message("请先登录");
                return;
            }
            let betAmount = 0;
            let weiShu = 0;
            let betCount = 0;
            //let beiShu = 0;
            for (var i= 0; i< this.CartData.length; i++){
				if(parseInt(this.CartData[i].beiShu) < this.config.min_bet)
				{
					this.message("单笔投注金额不得小于 " + this.config.min_bet);
					return;
				}
                betCount += this.CartData[i].betCount;
                betAmount += parseInt(this.CartData[i].beiShu);
				//beiShu = parseInt(this.CartData[i].beiShu);
                if(this.CartData[i].WeiShu)
                {
                    weiShu = this.CartData[i].WeiShu;
                }else
                {
                    weiShu = 0;
                }
                code.push({
                    actionData:this.CartData[i].betData,
                    playedId:this.CartData[i].playedId,
                    actionAmount:this.CartData[i].beiShu,
                    type:this.options.type_id,
                    weiShu:weiShu,
                });
            }
			if(betAmount > this.user.coin)
			{
                this.message("钱包余额不足,请先充值");
                return;
			}
            let Data = {
                code : code,
                para:{
                    type:this.options.type_id,
                    actionNo:this.currentIssue.actionNo,
                    kjTime:this.currentIssue.actionTime,
                    betType:1
                },
            };
			let loadingInstance = this.$loading(this.loadOptions);
            apiPostBetMax4D(Data)
            .then(response => {
                loadingInstance.close();
                if(response.data.error)
                {
                    this.error_handler(response.data);
                }
                if(response.data.status)
                {
                    this.$store.dispatch('setCoin',response.data.coin);
                    this.message(response.data.msg);
                    this.showme = false;
                }
				this.restBet();
                return;
            }).catch(error => {
				loadingInstance.close();
                console.log(error);
                return;
            });
        },
        RestChooseBet()
        {
            if(this.betFlag.length > 0)
            {
                for (var i= 0; i< this.betFlag.length; i++){
                    var target = this.betFlag[i].target;
					var input=target;
                    var str=target.className;
                    str=str.replace(" addbet", "");
                    target.className=str.replace(" addOF", "");
					input=target.childNodes[target.childNodes.length-1].childNodes[1];
					if(input)
					{
						input.value="";
					}
                }
            }
            this.$store.commit('RestChooseBet');
        },
		addCart()
		{
			for(var k in this.betData)
			{
				if(this.betData[k].betCount > 0)
				{
					var Data ={
						actionName:this.mapByPlay[this.betData[k].playedId].name,
						betData:this.betData[k].betData,
						bonus:this.mapByPlay[this.betData[k].playedId].bp,
						betCount:this.betData[k].betCount,
						beiShu:this.beiShu,
						actionAmount:this.beiShu * 2 * this.betData[k].betCount,
						playedId:this.betData[k].playedId,
					};
					if(this.betData[k].WeiShu)
					{
						Data.WeiShu = this.betData[k].WeiShu;
					}
					this.$store.commit('AddCartData',Data);
				}
			}
			this.RestChooseBet();
		},
    },
}
